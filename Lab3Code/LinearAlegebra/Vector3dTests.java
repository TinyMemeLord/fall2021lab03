//Shay Alex Lelichev 2043812
package LinearAlegebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {

    @Test
    public void testGet() {
        
        Vector3d vT1 = new Vector3d(2 , 5 , 7);
        assertEquals(2 , vT1.getX());
        assertEquals(5 , vT1.getY());
        assertEquals(7 , vT1.getZ());
    }

    @Test
    public void testMag() {
        
        Vector3d vT2 = new Vector3d(2, 3, 6);
        //magnitude() returns 7 for this vector.
        assertEquals(7 , vT2.magnitude());
    }

    @Test
    public void testDotPro() {
        
        Vector3d vT3_1 = new Vector3d(8 , 4 , 3);
        Vector3d vT3_2 = new Vector3d(5 , 2 , 9);
        //dotProduct returns 75 for these vectors.
        assertEquals(75 , vT3_1.dotProduct(vT3_2));
    }
    
    @Test
    public void testAdd() {
        
        Vector3d vT4_1 = new Vector3d(5 , 6 , 7);
        Vector3d vT4_2 = new Vector3d(2 , 3 , 4);
        //magnitude() returns 7 for this vector.
        assertEquals("(7.0 , 9.0 , 11.0)" , vT4_1.add(vT4_2).toString());
    }
}
