//Shay Alex Lelichev 2043812
package LinearAlegebra;

public class Vector3d {

    private double x;
    private double y;
    private double z;

    public Vector3d(double setX , double setY , double setZ) {

        this.x = setX;
        this.y = setY;
        this.z = setZ;
    }

    public double getX() {
        
        return this.x;
    }

    public double getY() {
        
        return this.y;
    }
    
    public double getZ() {
        
        return this.z;
    }

    public double magnitude() {

        return Math.sqrt(Math.pow(this.x , 2) + Math.pow(this.y , 2) + Math.pow(this.z , 2));
    }

    public double dotProduct(Vector3d vectToMult) {

        return (this.x * vectToMult.getX()) + (this.y * vectToMult.getY()) + (this.z * vectToMult.getZ());
    }

    public Vector3d add(Vector3d vectToAdd) {

        return new Vector3d(this.x + vectToAdd.getX() , this.y + vectToAdd.getY() , this.z + vectToAdd.getZ());
    }

    public String toString() {

        return "(" + this.x + " , " + this.y + " , " + this.z + ")";
    }
}